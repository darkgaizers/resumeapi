package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gorilla/mux"
)

type Resume struct {
	ID              string `json:"id"`
	Firstname       string `json:"firstname"`
	Lastname        string `json:"lastname"`
	Fulldisplayname string `json:"fulldisplayname"`
	Birthdate       time.Time
	birthdate       string `json:"birthdate"`

	Nationality string `json:"nationality"`
	Address     string `json:"address"`
	Contact     struct {
		Phone    string `json:"phone"`
		Email    string `json:"email"`
		Linkedin string `json:"linkedin"`
	} `json:"contact"`
	Info struct {
		Brief          string   `json:"brief"`
		Background     []string `json:"background"`
		Additionalinfo struct {
			Interest []string `json:"interest"`
		} `json:"additionalinfo"`
	} `json:"info"`
	Experience []struct {
		ID        string `json:"id"`
		Company   string `json:"company"`
		Startdate time.Time
		startdate string `json:"startdate"`
		Enddate   time.Time
		enddate   string `json:"enddate"`
		Position  string `json:"position"`
		Location  struct {
			Name         string `json:"name"`
			Locationlink string `json:"locationlink"`
		} `json:"location"`
		Icon        string   `json:"icon"`
		Description []string `json:"description"`
	} `json:"experience"`
	Skills []struct {
		Name        string `json:"name"`
		Proficiency int    `json:"proficiency"`
		Type        string `json:"type"`
	} `json:"skills"`
	Projects []struct {
		Name        string `json:"name"`
		Startdate   time.Time
		startdate   string `json:"startdate"`
		Enddate     time.Time
		enddate     string   `json:"enddate"`
		Description []string `json:"description"`
		Banner      string   `json:"banner"`
	} `json:"projects"`
	Media struct {
		Homescreen  string `json:"homescreen"`
		Indexscreen string `json:"indexscreen"`
		Divider     string `json:"divider"`
	} `json:"media"`
}

func (resume *Resume) UnmarshalJSON(file []byte) {
	err := json.Unmarshal(file, &resume)
	if err != nil {
		fmt.Println(err.Error())
	}

	layout := "2006-01-02T15:04:05.000Z"
	t, err := time.Parse(layout, resume.birthdate)
	if err == nil {
		resume.Birthdate = t
	}
	for _, exp := range resume.Experience {
		t, err = time.Parse(layout, exp.startdate)
		if err == nil {
			exp.Startdate = t
		}
		t, err = time.Parse(layout, exp.enddate)
		if err == nil {
			exp.Enddate = t
		}
	}
	for _, proj := range resume.Projects {
		t, err = time.Parse(layout, proj.startdate)
		if err == nil {
			proj.Startdate = t
		}
		t, err = time.Parse(layout, proj.enddate)
		if err == nil {
			proj.Enddate = t
		}
	}

}

var resume []Resume
var port string

func init() {
	port = "8000"
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/api", GetIndex).Methods("GET")
	router.HandleFunc("/api/resume/{user}", GetResume).Methods("GET")
	router.HandleFunc("/api/resume/exp/{user}/{sort}/{order}", GetResumeExp).Methods("GET")
	log.Printf("start at port %s!", port)
	log.Fatal(http.ListenAndServe(":"+string(port), router))
}
func GetIndex(w http.ResponseWriter, r *http.Request) {}
func GetResume(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	filename := fmt.Sprintf("./data/resume/%s.json", params["user"])
	fmt.Printf("file name %s\n", filename)
	file, e := ioutil.ReadFile(filename)
	if e != nil {
		fmt.Printf("File error: %v\n", e)
		os.Exit(1)
	}
	//fmt.Printf("%s\n", string(file))
	var resume Resume
	resume.UnmarshalJSON(file)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resume)
}
func GetResumeExp(w http.ResponseWriter, r *http.Request) {
	params := mux.Vars(r)
	file, e := ioutil.ReadFile(fmt.Sprintf("./data/resume/%s.json", params["user"]))

	if e != nil {
		fmt.Printf("File error: %v\n", e)
		os.Exit(1)
	}
	fmt.Printf("%s\n", string(file))
	var resume Resume
	resume.UnmarshalJSON(file)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(resume.Experience)
}
