FROM alpine

RUN apk add --no-cache ca-certificates && apk update && rm -rf /var/cache/apk/*
ADD main /main
ADD data /data
CMD ["./main"]